/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.webvendas.bean;

import br.com.senac.webvendas.dao.UsuarioDAO;
import br.com.senac.webvendas.model.Usuario;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;

@Named(value = "loginBean")
@SessionScoped
public class LoginBean extends Bean{

    private Usuario usuario = new Usuario();
    private Usuario usuarioLogado ; 
    private UsuarioDAO dao ;

    public LoginBean() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public String toHome(){
        
        dao = new UsuarioDAO() ; 
        
        usuarioLogado = dao.findByUsername(usuario.getNome()) ; 
        
        if(this.usuario.getSenha().equals(usuarioLogado.getSenha()) ){
            
            return "home";
        }
        
        
        
        return null;
    }

    public String getNomeUsuarioLogado() {
        return usuarioLogado.getNome();
    }

    public Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }
    
    
    
    

}
