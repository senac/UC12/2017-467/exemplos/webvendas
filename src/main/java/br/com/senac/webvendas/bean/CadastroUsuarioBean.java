
package br.com.senac.webvendas.bean;

import br.com.senac.webvendas.dao.UsuarioDAO;
import br.com.senac.webvendas.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "cadastroUsuarioBean")
@ViewScoped
public class CadastroUsuarioBean extends Bean{

    private Usuario usuario ; 
    private UsuarioDAO dao ; 
    private List<Usuario> usuarios ; 
    
    private void carregarLista(){
        this.usuarios =  this.dao.findAll() ; 
    }
    
    public CadastroUsuarioBean() {
        usuario = new Usuario();
        dao = new UsuarioDAO() ; 
        this.carregarLista();
    }
    
    
    public void salvar(){
        
        if(this.usuario.getId() == 0 ){
            dao.save(usuario);
            addMessageInfo("Salvo com sucesso!");
        }else{
            dao.update(usuario);
            addMessageInfo("Alterado com sucesso!");
        }
        
        
        this.carregarLista();
        
        
    }
    
    public void novo(){
        this.usuario = new Usuario();
    }
    
    

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
    
    
    
    
    
}
