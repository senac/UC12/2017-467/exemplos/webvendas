
package br.com.senac.webvendas.dao;

import br.com.senac.webvendas.model.Usuario;
import javax.persistence.NoResultException;
import javax.persistence.Query;


public class UsuarioDAO extends DAO<Usuario>{
    
    public UsuarioDAO() {
        super(Usuario.class);
    }
    
     public Usuario findByUsername (String userName){
        
         Usuario usuario = new Usuario() ; 
         try{
         this.em = JPAUtil.getEntityManager();
         
        
         Query query = this.em.createQuery("from Usuario u where u.nome = :userName ") ; 
         query.setParameter("userName", userName) ; 
         usuario = (Usuario)query.getSingleResult() ; 
         
         
         this.em.close();
         }catch(NoResultException ex){
             ex.printStackTrace();
             return usuario ; 
         }
         
        return usuario ; 
    }
    
    
    
}
